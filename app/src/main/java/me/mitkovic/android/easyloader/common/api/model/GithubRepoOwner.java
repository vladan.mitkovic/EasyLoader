
package me.mitkovic.android.easyloader.common.api.model;

public class GithubRepoOwner {

    private long id;
    private String avatar_url;

    public long getId() {
        return id;
    }

    public String getAvatar() {
        return avatar_url;
    }

    @Override
    public String toString() {
        return "GithubRepoOwner{" +
                "id=" + id +
                ", avatar_url='" + avatar_url + '\'' +
                '}';
    }
}
