package me.mitkovic.android.easyloader.common.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import me.mitkovic.android.easyloader.BuildConfig;
import me.mitkovic.android.easyloader.common.api.GitHubAPI;
import me.mitkovic.android.easyloader.common.di.scope.EasyLoaderApplicatonScope;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class APIModule {

    @Provides
    @EasyLoaderApplicatonScope
    public OkHttpClient providesOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        HttpLoggingInterceptor headerLogging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        HttpLoggingInterceptor bodyLogging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(headerLogging)
                .addInterceptor(bodyLogging)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @EasyLoaderApplicatonScope
    public Gson providesGson(GsonBuilder gsonBuilder) {
        return gsonBuilder.create();
    }

    @Provides
    @EasyLoaderApplicatonScope
    public GsonBuilder providesGsonBuilder() {
        return new GsonBuilder();
    }

    @Provides
    @EasyLoaderApplicatonScope
    public Retrofit retrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .baseUrl(BuildConfig.GITHUB_API_ENDPOINT)
                .build();
    }

    @Provides
    @EasyLoaderApplicatonScope
    public GitHubAPI githubService(Retrofit githubRetrofit) {
        return githubRetrofit.create(GitHubAPI.class);
    }

}
