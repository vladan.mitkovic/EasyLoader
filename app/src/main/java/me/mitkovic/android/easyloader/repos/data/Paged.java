package me.mitkovic.android.easyloader.repos.data;

import java.util.List;

public class Paged<T> {

    private int total;
    private List<T> items;

    public Paged(int total, List<T> items) {
        this.total = total;
        this.items = items;
    }

    public int getTotal() {
        return total;
    }

    public List<T> getItems() {
        return items;
    }

    public boolean hasMore() {
        return total > items.size();
    }

    @Override
    public String toString() {
        return "Paged{" +
                "total=" + total +
                ", items=" + items +
                '}';
    }
}
