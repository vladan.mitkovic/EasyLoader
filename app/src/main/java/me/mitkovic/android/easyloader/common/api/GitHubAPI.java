package me.mitkovic.android.easyloader.common.api;

import io.reactivex.Single;
import me.mitkovic.android.easyloader.common.api.model.GithubReposResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GitHubAPI {

    String QUERY = "mojombo";
    int PER_PAGE = 7;
    String SORT = "forks";
    String ORDER = "desc";

    @GET("search/repositories")
    Single<GithubReposResponse> getUserRepositories(@Query("q") String q,
                                                    @Query("page") int page, @Query("per_page") int perPage,
                                                    @Query("sort") String sort, @Query("order") String order);

}
