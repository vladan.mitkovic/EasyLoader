package me.mitkovic.android.easyloader.repos.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import me.mitkovic.android.easyloader.R;
import me.mitkovic.android.easyloader.common.api.model.GithubRepo;
import me.mitkovic.android.easyloader.common.view.EndlessRecyclerViewScrollListener;

import static me.mitkovic.android.easyloader.repos.view.ReposAdapter.State.HAS_MORE;

public class ReposActivity extends AppCompatActivity implements ReposView {

    @Inject
    ReposPresenter presenter;

    @BindView(R.id.repo_list)
    RecyclerView recyclerView;

    @Inject
    ReposAdapter reposAdapter;

    @BindView(R.id.loading)
    View loading;

    @BindView(R.id.layout_holder)
    RelativeLayout layoutHolder;

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        setContentView(R.layout.activity_repos);
        ButterKnife.bind(this);

        setTitle(getString(R.string.activity_repos));

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(reposAdapter);

        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (reposAdapter.getState() == HAS_MORE) {
                    presenter.onLoadMore();
                }
            }
        });

        if (savedInstanceState != null) {
            presenter.onRestore(savedInstanceState);
        }
        presenter.onBind(this);
    }

    @Override
    public void setAdapterData(final List<GithubRepo> githubRepos) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                reposAdapter.swapData(githubRepos);
            }
        });
    }

    @Override
    public void showState(final State state) {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                reposAdapter.setState(convertState(state));
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (presenter != null)
            presenter.onSave(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null)
            presenter.onUnbind();
    }

    private ReposAdapter.State convertState(ReposView.State state) {
        switch (state) {
            case HAS_MORE:
                return ReposAdapter.State.HAS_MORE;
            case DONE:
                return ReposAdapter.State.DONE;
            case LOADING:
                return ReposAdapter.State.LOADING;
            default:
                return ReposAdapter.State.ERROR;
        }
    }

}
