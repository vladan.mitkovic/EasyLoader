package me.mitkovic.android.easyloader.common.view;

import android.os.Bundle;

public interface Presenter<T extends View> {

    void onRestore(Bundle bundle);

    void onBind(T view);

    void onUnbind();

    void onSave(Bundle bundle);

}
