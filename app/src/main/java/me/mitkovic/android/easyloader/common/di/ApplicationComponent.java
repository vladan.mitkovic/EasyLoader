package me.mitkovic.android.easyloader.common.di;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import me.mitkovic.android.easyloader.common.EasyLoaderApplication;
import me.mitkovic.android.easyloader.common.di.module.APIModule;
import me.mitkovic.android.easyloader.common.di.module.AppModule;
import me.mitkovic.android.easyloader.common.di.module.ApplicationModule;
import me.mitkovic.android.easyloader.common.di.scope.EasyLoaderApplicatonScope;

@EasyLoaderApplicatonScope
@Component(modules = {
        AndroidInjectionModule.class,
        ApplicationModule.class,
        APIModule.class,
        AppModule.class
})
public interface ApplicationComponent {

    void inject(EasyLoaderApplication easyLoaderApplication);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(EasyLoaderApplication application);
        ApplicationComponent build();
    }

}
