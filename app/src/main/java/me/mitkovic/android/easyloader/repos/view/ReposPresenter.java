package me.mitkovic.android.easyloader.repos.view;

import me.mitkovic.android.easyloader.common.view.Presenter;

public interface ReposPresenter extends Presenter<ReposView> {

    void onLoadMore();

}
