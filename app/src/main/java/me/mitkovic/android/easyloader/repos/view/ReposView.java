package me.mitkovic.android.easyloader.repos.view;

import java.util.List;

import me.mitkovic.android.easyloader.common.api.model.GithubRepo;
import me.mitkovic.android.easyloader.common.view.View;

public interface ReposView extends View {

    enum State {
        LOADING, HAS_MORE, DONE, ERROR
    }

    void showState(State state);

    void setAdapterData(List<GithubRepo> githubRepos);

}
