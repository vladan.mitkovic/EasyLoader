
package me.mitkovic.android.easyloader.common.api.model;

public class GithubRepo {

    private int id;

    private String name;

    private GithubRepoOwner owner;

    private String description;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public GithubRepoOwner getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "GithubRepo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", owner=" + owner +
                ", description='" + description + '\'' +
                '}';
    }
}
