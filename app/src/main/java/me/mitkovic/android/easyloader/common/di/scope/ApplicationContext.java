package me.mitkovic.android.easyloader.common.di.scope;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
