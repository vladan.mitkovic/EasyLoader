package me.mitkovic.android.easyloader.repos.view;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import icepick.Icepick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import me.mitkovic.android.easyloader.common.api.GitHubAPI;
import me.mitkovic.android.easyloader.common.api.model.GithubRepo;
import me.mitkovic.android.easyloader.common.api.model.GithubReposResponse;
import me.mitkovic.android.easyloader.repos.data.Paged;
import me.mitkovic.android.easyloader.repos.view.ReposView.State;

public class ReposPresenterImpl implements ReposPresenter {

    private ReposView view;

    private GitHubAPI githubService;

    private CompositeDisposable subscription;
    private Disposable serviceSubscription;

    private List<GithubRepo> allRepos;

    private int currentPage = 0;

    @Inject
    public ReposPresenterImpl(GitHubAPI githubService) {
        this.githubService = githubService;
        allRepos = new ArrayList<>();
    }

    @Override
    @DebugLog
    public void onBind(final ReposView view) {
        this.view = view;

        view.showState(State.LOADING);

        subscription = new CompositeDisposable();

        getData();
    }

    private void getData() {
        serviceSubscription = subscribeToRepositories(loadMore());
        subscription.add(serviceSubscription);
    }

    @Override
    public void onLoadMore() {

        view.showState(State.LOADING);

        if (serviceSubscription != null) {
            serviceSubscription.dispose();
            serviceSubscription = null;
        }

        serviceSubscription = subscribeToRepositories(loadMore());
        subscription.add(serviceSubscription);
    }

    private Observable<Paged<GithubRepo>> loadMore() {
        final int page = currentPage += 1;
        return githubService
                .getUserRepositories(GitHubAPI.QUERY, page, GitHubAPI.PER_PAGE, GitHubAPI.SORT, GitHubAPI.ORDER)
                .map(pageGitHubResponse())
                .doOnSuccess(saveGithubRepos())
                .toObservable();
    }

    private Function<GithubReposResponse, Paged<GithubRepo>> pageGitHubResponse() {
        return new Function<GithubReposResponse, Paged<GithubRepo>>() {

            @Override
            @DebugLog
            public Paged<GithubRepo> apply(GithubReposResponse githubReposResponse) throws Exception {
                return new Paged<>(githubReposResponse.getTotal(), githubReposResponse.getGithubRepos());
            }
        };
    }

    private Consumer<Paged<GithubRepo>> saveGithubRepos() {
        return new Consumer<Paged<GithubRepo>>() {

            @Override
            @DebugLog
            public void accept(Paged<GithubRepo> githubRepoPaged) throws Exception {
                allRepos.addAll(githubRepoPaged.getItems());
            }
        };
    }

    private Disposable subscribeToRepositories(Observable<Paged<GithubRepo>> auctionsObservable) {
        return auctionsObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successConsumer(), errorConsumer());
    }

    private Consumer<Paged<GithubRepo>> successConsumer() {
        return new Consumer<Paged<GithubRepo>>() {

            @Override
            @DebugLog
            public void accept(Paged<GithubRepo> repoPaged) throws Exception {
                view.setAdapterData(allRepos);
                view.showState(repoPaged.hasMore() && (repoPaged.getItems().size() > 0) ?
                        State.HAS_MORE : State.DONE);
            }
        };
    }

    private Consumer<Throwable> errorConsumer() {
        return new Consumer<Throwable>() {

            @Override
            @DebugLog
            public void accept(Throwable throwable) throws Exception {
                view.showState(State.ERROR);
            }
        };
    }

    @Override
    public void onUnbind() {
        this.view = null;

        if (subscription != null) {
            subscription.dispose();
            subscription = null;
        }
    }

    @Override
    public void onRestore(Bundle bundle) {
        Icepick.restoreInstanceState(this, bundle);
    }

    @Override
    public void onSave(Bundle bundle) {
        Icepick.saveInstanceState(this, bundle);
    }

}
