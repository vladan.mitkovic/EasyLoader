package me.mitkovic.android.easyloader.common.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import me.mitkovic.android.easyloader.common.di.scope.PerActivity;
import me.mitkovic.android.easyloader.repos.di.ReposModule;
import me.mitkovic.android.easyloader.repos.view.ReposActivity;

@Module
public abstract class AppModule {

    @PerActivity
    @ContributesAndroidInjector(modules = ReposModule.class)
    abstract ReposActivity reposActivityInjector();
}
