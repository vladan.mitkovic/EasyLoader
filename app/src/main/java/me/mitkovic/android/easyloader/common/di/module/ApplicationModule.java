package me.mitkovic.android.easyloader.common.di.module;

import android.app.Activity;
import android.content.Context;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import me.mitkovic.android.easyloader.common.EasyLoaderApplication;
import me.mitkovic.android.easyloader.common.di.scope.ApplicationContext;
import me.mitkovic.android.easyloader.common.di.scope.EasyLoaderApplicatonScope;

@Module
public abstract class ApplicationModule {

    @EasyLoaderApplicatonScope
    @Binds
    abstract AndroidInjector<Activity> bindsActivityInjector(DispatchingAndroidInjector<Activity> injector);

    @Provides
    @EasyLoaderApplicatonScope
    @ApplicationContext
    public static Context providesContext(EasyLoaderApplication application) {
        return application.getApplicationContext();
    }

}
