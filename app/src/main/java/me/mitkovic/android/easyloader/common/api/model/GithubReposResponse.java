package me.mitkovic.android.easyloader.common.api.model;

import java.util.List;

public class GithubReposResponse {

    private List<GithubRepo> items;
    private int total_count;

    private GithubReposResponse(List<GithubRepo> githubRepos, int total) {
        this.items = githubRepos;
        this.total_count = total;
    }

    public List<GithubRepo> getGithubRepos() {
        return items;
    }

    public int getTotal() {
        return total_count;
    }

    @Override
    public String toString() {
        return "GithubReposResponse{" +
                "items=" + items +
                ", total_count=" + total_count +
                '}';
    }
}
