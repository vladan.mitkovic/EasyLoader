package me.mitkovic.android.easyloader.repos.view;


import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.mitkovic.android.easyloader.R;
import me.mitkovic.android.easyloader.common.api.model.GithubRepo;
import me.mitkovic.android.easyloader.repos.view.ReposActivity.ItemClickListener;

public class ReposAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemClickListener {

    private static final int TYPE_REPO = 0;
    private static final int TYPE_LOADING = 1;
    private static final int TYPE_ERROR = 2;
    private static final int TYPE_NO_MORE_RESULTS = 3;

    public enum State {
        LOADING, ERROR, HAS_MORE, DONE
    }

    private State state = State.LOADING;

    private List<GithubRepo> repoList = new ArrayList<>();
    private List<Integer> itemTypes = new ArrayList<>();

    private LayoutInflater layoutInflater;
    private ReposActivity context;

    @Inject
    public ReposAdapter(ReposActivity context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case TYPE_REPO:
                return new ReposViewHolder(layoutInflater.inflate(R.layout.list_item_repo, viewGroup, false), this);
            case TYPE_ERROR:
                return new SimpleViewHolder(layoutInflater.inflate(R.layout.repos_error, viewGroup, false));
            case TYPE_LOADING:
                return new SimpleViewHolder(layoutInflater.inflate(R.layout.repos_loading, viewGroup, false));
            case TYPE_NO_MORE_RESULTS:
                return new SimpleViewHolder(layoutInflater.inflate(R.layout.repos_no_more_results, viewGroup, false));
        }

        throw new RuntimeException("Unknown viewType");
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof ReposViewHolder) {
            ReposViewHolder viewHolder = ((ReposViewHolder) holder);
            viewHolder.name.setText(repoList.get(i).getName());
            viewHolder.description.setText(repoList.get(i).getDescription());
            viewHolder.useAvatar.setImageURI(Uri.parse(repoList.get(i).getOwner().getAvatar()));
        }
    }

    @Override
    public int getItemCount() {
        return itemTypes.size();
    }

    @Override
    public int getItemViewType(int position) {
        return itemTypes.get(position);
    }

    public void swapData(List<GithubRepo> githubRepos) {
        repoList.clear();
        if(githubRepos != null) {
            repoList.addAll(githubRepos);
        }
        notifyDataSetChanged();
    }

    public void setState(State state) {
        this.state = state;

        calculateIndexes();
        notifyDataSetChanged();
    }

    public State getState() {
        return state;
    }

    private void calculateIndexes() {
        itemTypes.clear();

        if (repoList.size() > 0) {
            for (int i = 0; i < repoList.size(); i++) {
                itemTypes.add(TYPE_REPO);
            }
        }

        switch (state) {
            case LOADING:
                itemTypes.add(TYPE_LOADING);
                break;
            case ERROR:
                itemTypes.add(TYPE_ERROR);
                break;
            case DONE:
                itemTypes.add(TYPE_NO_MORE_RESULTS);
                break;
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(context, context.getString(R.string.repository_name_message) + repoList.get(position).getName(), Toast.LENGTH_SHORT).show();
    }

    public class ReposViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.repo_name)
        TextView name;

        @BindView(R.id.user_avatar)
        SimpleDraweeView useAvatar;

        @BindView(R.id.repo_description)
        TextView description;

        ItemClickListener itemClickListener;

        public ReposViewHolder(View view, ItemClickListener itemClickListener) {
            super(view);
            ButterKnife.bind(this, view);

            this.itemClickListener = itemClickListener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(view, getAdapterPosition());
        }

    }

}
