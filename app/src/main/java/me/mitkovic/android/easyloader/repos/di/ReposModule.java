package me.mitkovic.android.easyloader.repos.di;


import dagger.Binds;
import dagger.Module;
import me.mitkovic.android.easyloader.common.di.scope.PerActivity;
import me.mitkovic.android.easyloader.repos.view.ReposPresenter;
import me.mitkovic.android.easyloader.repos.view.ReposPresenterImpl;

@PerActivity
@Module
public abstract class ReposModule {

    @Binds
    abstract ReposPresenter bindsSimplePresenter(ReposPresenterImpl presenter);

}
